<?php

namespace App\Models;

use App\Models\Traits\Attributes\BlogAttributes;
use App\Models\Traits\ModelAttributes;
use App\Models\Traits\Relationships\BlogRelationships;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends BaseModel
{
    use ModelAttributes, SoftDeletes, BlogAttributes, BlogRelationships;

    /**
     * Fillable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'publish_datetime',
        'content',
        'meta_title',
        'cannonical_link',
        'meta_keywords',
        'meta_description',
        'status',
        'featured_image',
        'created_by',
        'updated_by',
    ];

    /**
     * Dates.
     *
     * @var array
     */
    protected $dates = [
        'publish_datetime',
        'created_at',
        'updated_at',
    ];

    /**
     * Statuses.
     *
     * @var array
     */
    protected $statuses = [
        0 => 'InActive',
        1 => 'Published',
        2 => 'Draft',
        3 => 'Scheduled',
    ];

    /**
     * Appends.
     *
     * @var array
     */
    protected $appends = [
        'display_status',
    ];

    public function blogCategories()
    {
        return $this->belongsToMany(BlogCategory::class, 'blog_map_categories', 'blog_id','category_id')->select(array('name'));
    }

    public function blogTags()
    {
        return $this->belongsToMany(BlogTag::class, 'blog_map_tags', 'blog_id','tag_id')->select(array('name'));
    }

    public function latestBlogTags()
    {
        return $this->belongsToMany(BlogTag::class, 'blog_map_tags', 'blog_id','tag_id')->orderBy('blog_map_tags.id')->limit(6)->select(array('name'));
    }

    public function user()
    {
        return $this->hasOne('App\Models\Auth\User','id', 'user_id');
    }
}
