<?php

namespace App\Models;

class BlogMapTag extends BaseModel
{
    public $timestamps = false;

    public function blogs()
    {
        return $this->belongsToMany(Blog::class, 'blog_map_tags', 'tag_id','blog_id');
    }
}
