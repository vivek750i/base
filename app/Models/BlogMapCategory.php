<?php

namespace App\Models;

class BlogMapCategory extends BaseModel
{
    public $timestamps = false;

    public function blogs()
    {
        return $this->belongsToMany(Blog::class, 'blog_map_categories', 'category_id','blog_id');
    }

}
