<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Blog;
/**
 * Class HomeController.
 */
class BlogController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index($slug='')
    {
        $categories=[];
        $latest_tags=[];
        if($slug)
        {
            $post = Blog::where('slug', $slug)->with('blogCategories','blogTags','user')->first();
            $latest_posts= Blog::with('user','blogTags')->orderBy('publish_datetime', 'DESC')->limit(3)->get();
            foreach($post->blogTags as $Tag)
            {
                $latest_tags[]= $Tag->name;
            }
            foreach($post->blogCategories as $category)
            {
                $categories[]= $category->name;
            }
            return view('frontend.single-blog')->with(['post'=>$post,'categories'=>$categories,'latest_posts'=>$latest_posts,'latest_tags'=>$latest_tags]);
        }
        $posts = Blog::orderBy('publish_datetime', 'DESC')->paginate(6);
        return view('frontend.blogs')->with('posts',$posts);
    }

}
