<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTourCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',500);
            $table->string('slug',500);
            $table->text('description');
            $table->string('image',500);
            $table->decimal('starting_price_aed',10,2);
            $table->decimal('starting_price_usd',10,2);
            $table->boolean('has_shared_car')->default(0);
            $table->boolean('has_private_car')->default(0);
            $table->boolean('has_tour_addons')->default(0);
            $table->boolean('is_active')->default(1);
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
            $table->unique(['slug']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_categories');
    }
}
