@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.blog'))

@section('content')

        <div class="blog-single">
            <div class="container">
                <div class="row align-items-start">
                    <div class="col-lg-8 m-15px-tb">
                        <article class="article">
                            <div class="article-img">
                                <img src="https://via.placeholder.com/800x350/87CEFA/000000" title="" alt="">
                            </div>
                            <div class="article-title">
                                <h6><span class="color-red">Categories:</span> {{implode(', ',$categories)}}</h6>
                                <h2>{{ucfirst($post->name)}}</h2>
                                <div class="media">
                                    <div class="avatar">
                                        @if($post->featured_image)
                                            <img src="{{url('/').'/'.$post->featured_image}}" title="" alt="{{ucfirst($post->name)}}">
                                        @else
                                            <img src="https://bootdey.com/img/Content/avatar/avatar1.png" title="" alt="">
                                        @endif
                                    </div>
                                    <div class="media-body">
                                        <label>{{$post->user->first_name.' '.$post->user->last_name}}</label>
                                        <span>{{$post->created_at->format('d M , Y')}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="article-content">
                                <p>{{$post->content}}</p>
                            </div>
                            <div class="nav tag-cloud">
                                <h6><span class="color-red mr-2">Tags:</span></h6>
                                @foreach($post->blogTags as $tag)
                                    <a href="#">{{$post->name}}</a>
                                @endforeach
                            </div>
                        </article>
                        <div class="contact-form article-comment">
                            <h4>Leave a Reply</h4>
                            <form id="contact-form" method="POST">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input name="Name" id="name" placeholder="Name *" class="form-control" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input name="Email" id="email" placeholder="Email *" class="form-control" type="email">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea name="message" id="message" placeholder="Your message *" rows="4" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="send">
                                            <button class="px-btn theme"><span>Submit</span> <i class="arrow"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-4 m-15px-tb blog-aside">
                        <!-- Latest Post -->
                        <div class="widget widget-latest-post">
                            <div class="widget-title">
                                <h3>Latest Post</h3>
                            </div>
                            <div class="widget-body">
                                @foreach($latest_posts as $latest_post)
                                    <div class="latest-post-aside media">
                                        <div class="lpa-left media-body">
                                            <div class="lpa-title">
                                                <h5><a href="#">{{$latest_post->name}}</a></h5>
                                            </div>
                                            <div class="lpa-meta">
                                                <a class="name" href="#">
                                                    {{$latest_post->user->first_name.' '.$latest_post->user->last_name}}
                                                </a>
                                                <a class="date" href="#">
                                                    {{$latest_post->created_at->format('d M , Y')}}
                                                </a>
                                            </div>
                                        </div>
                                        <div class="lpa-right">
                                            <a href="#">
                                                @if($latest_post->featured_image)
                                                    <img src="{{url('/').'/'.$latest_post->featured_image}}" title="" alt="{{ucfirst($latest_post->name)}}">
                                                @else
                                                    <img src="https://via.placeholder.com/400x200/FFB6C1/000000" title="" alt="">
                                                @endif
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- End Latest Post -->
                        <!-- widget Tags -->
                        <div class="widget widget-tags">
                            <div class="widget-title">
                                <h3>Latest Tags</h3>
                            </div>
                            <div class="widget-body">
                                <div class="nav tag-cloud">
                                    @foreach($latest_tags as $lastest_tag)
                                        <a href="#">{{$lastest_tag}}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!-- End widget Tags -->
                    </div>
                </div>
            </div>
        </div>

@endsection
